package main

import "fmt"

func main() {
	fmt.Println(removeDuplicates([]int{1, 1, 2}))
	fmt.Println(removeDuplicates([]int{0, 0, 1, 1, 1, 2, 2, 3, 3, 4}))
}

func removeDuplicates(nums []int) int {
	if len(nums) <= 1 {
		return len(nums)
	} else {
		result, temp := 1, nums[0]

		for i := 1; i < len(nums); i++ {
			if nums[i] != temp {
				temp = nums[i]

				nums[result] = temp
				result++
			}
		}

		return result
	}
}
